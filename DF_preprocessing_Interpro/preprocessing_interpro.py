import pandas as pd
from Bio import SeqIO
from Bio.Seq import Seq
#from Bio.Alphabet import generic_protein
from Bio.SeqRecord import SeqRecord

import numpy as np
import argparse
import gzip
import csv

def preprocessing(file):
    df = pd.read_csv(file, delimiter = '\t')
    df_set_final = set(df["INTERPRO_ID"])
    df['PDB-ID'] = df['PDB'].astype(str) + '-' + df['CHAIN'].astype(str)
    del df['CHAIN']
    del df['PDB']
    df = df.groupby('PDB-ID')['INTERPRO_ID'].apply(','.join).reset_index()
    df.to_csv('pdb_chain_interpro_final.tsv', sep="\t", index=False)
    with open('interpro_id_final_unique.tsv', 'wt') as out_file:
        tsv_writer = csv.writer(out_file, delimiter='\t')
        tsv_writer.writerow(df_set_final)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-file', type=str, help="output from interpro run.")
    args = parser.parse_args() 

interpro = preprocessing(args.file)   
